package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Timer;
import java.util.TimerTask;


public class MyGame extends ApplicationAdapter {
	Stage stage;
	TextButton textButton;
	Table table;
	Label label;
	String text;
	Skin uiSkin;
	int counter;
	Timer timer;
	OrthographicCamera camera;
	Viewport viewport;

	@Override
	public void create () {

		camera = new OrthographicCamera();
		viewport = new ExtendViewport(720,360,camera);					// use viewport to fit text size to screen resolution
		viewport.apply();
		camera.position.set(camera.viewportWidth/2,camera.viewportHeight/2,0);

		stage = new Stage(viewport);

		uiSkin = new Skin(Gdx.files.internal("uiskin.json"));									//use default uiskin.json
		textButton = new TextButton("start",uiSkin);
		label = new Label("",uiSkin);

		table = new Table();																		//use table like a container for our button and label (also we can use .debug() to make it visible)
		table.setBounds((int) ((stage.getWidth()/2)- (stage.getHeight()*0.9)/2),					//set size to 90% from screen height and centered it
				(int) (stage.getHeight()-stage.getHeight()*0.9)/2,
				(int) (stage.getHeight()*0.9),
				(int) (stage.getHeight()*0.9));

		text = 	"Lorem ipsum dolor sit amet,\n" +
				"consectetur adipiscing elit,\n" +
				"sed do eiusmod tempor incididunt\n" +
				"ut labore et dolore magna aliqua.";

		label.setAlignment(Align.center);
		textButton.addListener(new ChangeListener() {

			@Override
			public void changed (ChangeEvent event, Actor actor) {
				counter=0;
				if(timer!= null)
				{
					timer.cancel();
				}
				timer  = new Timer();
				timer.scheduleAtFixedRate(new TimerTask() {
					@Override
					public void run() {
						label.setText(text.substring(0,counter));								//.substring() text with every tick of the task with 100ms rate (symbol per 100ms and 10 symbols per 1000ms (1 sec))
						if(counter< text.length())
						{
							counter++;
						}
					}
				},0,100);
			}
		});
		table.add(label)
				.expand()
				.top()
				.fillX()																			//label width same as container width
				.padTop((float)(table.getHeight()*0.1));											//top padding 1% from container height
		table.row();
		table.add(textButton)
				.expand()
				.bottom()
				.fillX()																			//button width same as container width
				.padLeft((float)(table.getHeight()*0.01))											//left padding 1% from container height
				.padRight((float)(table.getHeight()*0.01))											//right padding 1% from container height
				.padBottom((float)(table.getHeight()*0.01))											//bottom padding 1% from container height
				.height((float)(table.getHeight()*0.2));											//button height 20% from container height
		stage.addActor(table);
	}

	@Override
	public void render () {
		camera.update();
		Gdx.gl.glClearColor((float)208/255,													//clearing screen with selected color
							(float)227/255,
							(float)243/255,
							(float)0);
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
		Gdx.input.setInputProcessor(stage);
		stage.act();
		stage.draw();
	}

	@Override
	public void dispose () {
		stage.dispose();
	}

	@Override
	public void resize(int width, int height){
		viewport.update(width,height);
		camera.position.set(camera.viewportWidth/2,camera.viewportHeight/2,0);
	}
}
